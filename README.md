NCGC/NCATS Structure Search Server
==================================

This is a self-contained server and webclient for substructure
searching.  A live running version of this code is available
at [http://tripod.nih.gov/pcs/](http://tripod.nih.gov/pcs/). To run the
server from source, you'll need [Apache Ant](http://ant.apache.org).

```
ant run-server
```

If all goes well (and it should), you should be able to point your
browser to [http://localhost:8888/](http://localhost:8888). Now simply
upload you structure files (or via URL) and you're ready to go.  If
your structures are in the database, please customize the file
`JdbcSearchService.java` to reflect your local environment (i.e.,
schema, database driver, etc.).  If you have any problems, please feel
free to [contact us](mailto:nguyenda@mail.nih.gov).


Disclaimer
==========
                         PUBLIC DOMAIN NOTICE
                     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software is freely available to the
public for use. The NIH Chemical Genomics Center (NCGC) and the
U.S. Government have not placed any restriction on its use or
reproduction.

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

