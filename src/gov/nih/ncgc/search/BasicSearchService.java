/*
			 PUBLIC DOMAIN NOTICE
		     NIH Chemical Genomics Center
	       National Human Genome Research Institute

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/

package gov.nih.ncgc.search;

import java.util.concurrent.*;
import java.util.logging.*;
import java.io.*;
import java.net.URI;
import java.util.zip.*;

import chemaxon.struc.Molecule;
import chemaxon.util.MolHandler;
import chemaxon.formats.MolImporter;

import net.sf.ehcache.Element;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

public class BasicSearchService extends SearchService {
    static final Logger logger = 
	Logger.getLogger(BasicSearchService.class.getName());
    
    public BasicSearchService () {
    }

    public BasicSearchService (int nthreads) {
	super (nthreads);
    }

    public BasicSearchService (int nthreads, int maxdepth) {
	super (nthreads, DEFAULT_FP_SIZE, maxdepth);
    }

    /**
     * InputStream must be a valid mol input stream
     */
    public void load (InputStream is) throws Exception {
	MolImporter mi;
	try {
	    mi = new MolImporter (new GZIPInputStream (is));
	}
	catch (IOException ex) {
	    // not GZIP stream
	    mi = new MolImporter (is);
	}

	for (Molecule m; (m = mi.read()) != null; ) {
	    m.hydrogenize(false);
	    m.aromatize();
	    add (m);
	}
	mi.close();
    }

    @Override
    public void load (String addr) throws Exception {
	URI uri;
	File file = new File (addr);
	if (file.exists()) {
	    uri = file.toURI();
	}
	else {
	    uri = new URI (addr);
	}
	logger.info("Loading "+uri+"...");
	load (uri.toURL().openStream());
    }
}
