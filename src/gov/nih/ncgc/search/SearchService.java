/*
			 PUBLIC DOMAIN NOTICE
		     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/
package gov.nih.ncgc.search;

import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import chemaxon.sss.search.*;
import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.util.MolHandler;


public class SearchService {
    private static final Logger logger = 
	Logger.getLogger(SearchService.class.getName());

    protected static final int DEFAULT_THREAD_COUNT = 2;
    protected static final int DEFAULT_MAXDEPTH = 10;
    protected static final int DEFAULT_FP_SIZE = 16; // 16*32 = 512 bits
    protected static final int DEFAULT_FP_DARKNESS = 2;// 2 bits for each pattern
    protected static final int DEFAULT_FP_DEPTH = 6; // recursion depth

    // hard limit on search results
    protected static final int DEFAULT_SCREEN_LIMIT = 
	Integer.getInteger("screen-limit", 10000); 


    static class IndexValue {
	Object key;
	int[] value;

	IndexValue () {}
	IndexValue (Object key, int[] value) {
	    this.key = key;
	    this.value = value;
	}
    }

    static final IndexValue SENTINEL = new IndexValue ();


    static class IndexBuilder implements Runnable {
	BlockingQueue<IndexValue> queue;
	SignatureIndex indexer;

	IndexBuilder (BlockingQueue<IndexValue> queue, 
		      SignatureIndex indexer) {
	    if (queue == null) {
		throw new IllegalArgumentException ("Queue is null!");
	    }
	    this.queue = queue;
	    if (indexer == null) {
		throw new IllegalArgumentException ("Index is null");
	    }
	    this.indexer = indexer;
	}

	public void run () {
	    String name = "Thread-"+getClass().getName();
	    Thread.currentThread().setName(name);
	    try {
		for (IndexValue iv; (iv = queue.take()) != SENTINEL; ) {
		    indexer.add(iv.key, iv.value);
		}
		logger.info("Shutting down thread "+name);
	    }
	    catch (InterruptedException ex) {
		logger.info("Thread "+name+" interrupted!");
	    }
	}

	SignatureIndex getIndex () { return indexer; }
    }

    class IndexScreener implements Callable<List>, SearchCallback {
	SignatureIndex indexer;
	List results = new ArrayList ();
	int[] query;

	IndexScreener (int[] query, SignatureIndex indexer) {
	    this.query = query;
	    this.indexer = indexer;
	}

	public List call () {
	    SignatureIndex.SearchStats stats = indexer.search(query, this);
	    return results;
	}

	// SignatureIndex.SearchVisitor interface
	public boolean matched (Object key) {
	    results.add(key);
	    return true;
	}
    }

    class IndexSearcher implements Callable<List>, SearchCallback {
	SignatureIndex indexer;
	List results = new ArrayList ();
	Molecule query;
	int[] fp;
	MolSearch msearch = new MolSearch ();

	IndexSearcher (Molecule query, int[] fp, SignatureIndex indexer) {
	    this.query = query;
	    this.indexer = indexer;
	    this.fp = fp;
	    msearch.setQuery(query);
	}
	
	public List call () {
	    indexer.search(fp, this);
	    return results;
	}

	public boolean matched (Object key) {
	    Molecule mol = getMol (key);
	    if (mol == null) 
		return true;

	    msearch.setTarget(mol);
	    try {
		int[][] hits = msearch.findAll();
		if (hits != null) {
		    Molecule m = mol.cloneMolecule();
		    m.dearomatize();
		    for (int[] h : hits) {
			for (int i = 0; i < h.length; ++i) {
			    m.getAtom(h[i]).setAtomMap(i+1);
			}
		    }
		    results.add(m);
		}
	    }
	    catch (SearchException ex) {
		logger.log(Level.SEVERE, 
			   "MolSearch fails for query "+query.getName(), ex);
	    }
	    return true;
	}
    }


    class IndexSearchProducer implements Callable<Integer>, SearchCallback {

	SignatureIndex indexer;
        final SearchCallback<Molecule> callback;
        final CountDownLatch signal;
	Molecule query;
	MolSearch msearch = new MolSearch ();
	int screen = 0, matches = 0, limit;
	int []fp;
        boolean exact;

	IndexSearchProducer (Molecule query, int[] fp, boolean exact, 
                             int limit, CountDownLatch signal, 
                             SearchCallback<Molecule> callback, 
			     SignatureIndex indexer) {
	    this.query = query;
            this.signal = signal;
	    this.callback = callback;
	    this.indexer = indexer;
	    this.fp = fp;
	    this.limit = limit;
            this.exact = exact;
	    msearch.setQuery(query.cloneMolecule());
	}
	
	public Integer call () {
            try {
                logger.info(Thread.currentThread().getName()
                            +": start fingerprint screening!");
                
                indexer.search(fp, exact, this);

                // now identify this producer is finished                
                logger.info(Thread.currentThread().getName()
                            +" finished; screened="
                            +screen+" matched="+matches);
                
                return matches;
            }
            finally {
                signal.countDown();
            }
	}

	public boolean matched (Object key) {
            ++screen;

	    Molecule mol = getMol (key);
	    if (mol == null) 
		return true;
	    
	    msearch.setTarget(mol.cloneMolecule());
	    try {
		int[] h = msearch.findFirst();
		if (h != null) {
                    //logger.info(Thread.currentThread().getName()+": matched "+key);
		    ++matches;
		    
		    Molecule m = msearch.getTarget();
		    m.dearomatize();
                    for (int i = 0; i < h.length; ++i) {
                        if (h[i] >= 0) // explicit Hs
                            m.getAtom(h[i]).setAtomMap(i+1);
                    }
                    
                    if (screen < getScreenLimit () 
                        && (matches < limit || limit == 0)) {
                    }
                    else {
                        logger.warning
                            ("** Query "+query.toFormat("cxsmarts") +
                             " exceeds screen limit "+getScreenLimit ()
                             +" or match limit "+limit);
                        
                        return false;
                    }

                    return callback.matched(m);
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Search fails for query "+query.getName(), ex);
	    }

	    return screen < getScreenLimit ();
	}

	int getScreenCount () { return screen; }
	int getMatchCount () { return matches; }
    }

    class IndexCounter implements Callable<SignatureIndex.SearchStats> {
	SignatureIndex indexer;
	int[] query;
	
	IndexCounter (int[] query, SignatureIndex indexer) {
	    this.query = query;
	    this.indexer = indexer;
	}

	public SignatureIndex.SearchStats call () {
	    return indexer.search(query, null);
	}
    }

    /**
     * Input queue
     */
    private BlockingQueue<IndexValue> queue = 
	new LinkedBlockingQueue<IndexValue>();

    private int dim = DEFAULT_FP_SIZE; // in ints
    private ExecutorService threadPool;
    private IndexBuilder[] indexes;
    private volatile int screenLimit = DEFAULT_SCREEN_LIMIT;

    public SearchService () {
	this (DEFAULT_THREAD_COUNT);
    }

    public SearchService (int nthreads) {
	this (nthreads, DEFAULT_FP_SIZE);
    }

    public SearchService (int nthreads, int dim) {
	this (nthreads, dim, DEFAULT_MAXDEPTH);
    }

    public SearchService (int nthreads, int dim, int maxdepth) {
	threadPool = Executors.newCachedThreadPool();
	indexes = new IndexBuilder[nthreads];
	for (int i = 0; i < indexes.length; ++i) {
	    SignatureIndex index = new SignatureIndex (dim*32, maxdepth);
	    indexes[i] = new IndexBuilder (queue, index);
	    threadPool.submit(indexes[i]);
	}
	this.dim = dim;
    }

    public int getDim () { return dim; }
    public void setScreenLimit (int limit) {
	this.screenLimit = limit;
    }
    public int getScreenLimit () { return screenLimit; }

    public void shutdown () {
	try {
	    for (int i = 0; i < indexes.length; ++i) {
		logger.info("Shutting down index "+i+"\n"
			    +indexes[i].getIndex().getIndexStats());
		queue.offer(SENTINEL);
	    }
	    threadPool.shutdownNow();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Shutting down", ex);
	}
    }

    /**
     * subclass should provide a default implementation to perform
     * bulk loading for building the indexes
     */
    public void load (String resource) throws Exception {
    }

    public SignatureIndex.IndexStats[] getStats () {
	List<SignatureIndex.IndexStats> stats = 
	    new ArrayList<SignatureIndex.IndexStats>();
	for (int i = 0; i < indexes.length; ++i) {
	    SignatureIndex.IndexStats s = 
		indexes[i].getIndex().getIndexStats();
	    if (s != null) {
		stats.add(s);
	    }
	}
	return stats.toArray(new SignatureIndex.IndexStats[0]);
    }

    public void add (Object key, int[] index) {
	if (index == null || index.length != dim) {
	    throw new IllegalArgumentException ("Invalid index");
	}
	try {
	    queue.put(new IndexValue (key, index));
	}
	catch (InterruptedException ex) {
	    logger.log(Level.SEVERE, "Unable to queue input: "+key);
	}
    }

    static int[] generateFingerprint (Molecule mol, int dim) {
	MolHandler mh = new MolHandler (mol);
	mh.aromatize();
	return mh.generateFingerprintInInts
	    (dim, DEFAULT_FP_DARKNESS, DEFAULT_FP_DEPTH);
    }

    static Molecule createMol (String str) {
	try {
	    MolHandler mh = new MolHandler (str);
	    return mh.getMolecule();
	}
	catch (Exception ex) {
	    throw new IllegalArgumentException ("Invalid molecule");
	}
    }

    public void add (String str) {
	add (createMol (str));
    }

    public void add (Molecule mol) {
	add (mol, generateFingerprint (mol, dim));
    }

    public List search (String str) {
	return search (createMol (str));
    }

    public List search (String str, int limits) {
	return search (createMol (str), limits);
    }

    public List search (Molecule mol) {
	return search (mol, 0);
    }

    public List search (Molecule query, int limits) {
	Future[] tasks = new Future[indexes.length];
	int[] fp = generateFingerprint (query, getDim ());
	for (int i = 0; i < indexes.length; ++i) {
	    tasks[i] = threadPool.submit
		(new IndexSearcher (query, fp, indexes[i].getIndex()));
	}
	return getResults (limits, tasks);
    }

    public int count (Molecule query ) {
	int[] fp = generateFingerprint (query, getDim ());
	Future[] tasks = new Future[indexes.length];
	for (int i = 0; i < indexes.length; ++i) {
	    tasks[i] = threadPool.submit
		(new IndexCounter (fp, indexes[i].getIndex()));
	}

	// now accumulate the stats
	int total = 0;
	for (Future f : tasks) {
	    try {
		SignatureIndex.SearchStats s = 
		    (SignatureIndex.SearchStats)f.get();
		total += s.getHitCount();
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Thread interrupted while "
			   +"waiting for filter results!", ex);
	    }
	}
        logger.info("## Query="+query.toFormat("cxsmarts")+" => "+total);

	return total;
    }

    public int search (Molecule query, int limit, 
		       SearchCallback<Molecule> callback) {
        return search (query, limit, false, callback);
    }

    public int search (Molecule query, int limit, boolean exact,
		       SearchCallback<Molecule> callback) {
	if (callback == null) {
	    return count (query);
	}

        logger.info("## "+Thread.currentThread().getName()
                    +": query="+query.toFormat("cxsmarts"));

	IndexSearchProducer[] producers = 
	    new IndexSearchProducer[indexes.length];

	List<Future<Integer>> tasks = new ArrayList<Future<Integer>>();
	int[] fp = generateFingerprint (query, getDim ());

        CountDownLatch signal = new CountDownLatch (indexes.length);
	for (int i = 0; i < indexes.length; ++i) {
	    producers[i] = new IndexSearchProducer 
		(query, fp, exact, limit, signal, 
                 callback, indexes[i].getIndex());
	    tasks.add(threadPool.submit(producers[i]));
	}

        int matches = 0;
        try {
            // wait here
            signal.await();
            
            for (Future<Integer> f : tasks) {
                matches += f.get();
            }
        }
        catch (Exception ex) {
            logger.warning("Thread interrupted; search result "
                           +"might not be accurate!");
        }

	return matches;
    }

    public List filter (int[] query) {
	return filter (query, 0);
    }

    public List filter (int[] query, int limits) {
	Future[] tasks = new Future[indexes.length];
	for (int i = 0; i < indexes.length; ++i) {
	    tasks[i] = threadPool.submit
		(new IndexScreener (query, indexes[i].getIndex()));
	}
	return getResults (limits, tasks);
    }

    List getResults (int limits, Future... tasks) {
	List results = new ArrayList();
	for (Future f : tasks) {
	    try {
		List vals = (List)f.get();
		if (limits > 0 && results.size() >= limits) {
		    f.cancel(true);
		}
		else if (limits > 0) {
		    for (int i = results.size(), j = 0; 
			 i < limits && j < vals.size(); ++i, ++j) {
			results.add(vals.get(j));
		    }
		}
		else {
		    results.addAll(vals);
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Thread exception", ex);
	    }
	}
	return results;
    }

    /**
     * Sub class should provide a mechanism to retrieve a Molecule
     * instance given a key object that was used during the construction
     * of the indexes.  This method must be thread safe as it's called
     * from multiple threads!
     */
    protected Molecule getMol (Object key) {
	if (key instanceof Molecule) {
	    return (Molecule)key;
	}
	return null;
    }

    /********************
     ** TESTING 
     *******************/

    static int[] randomFp (java.util.Random rand, int dim) {
	return randomFp (rand, dim, rand.nextDouble());
    }

    static int[] randomFp (java.util.Random rand, int dim, double density) {
	int[] fp = new int[dim];
	if (density < 0) density *= -1.;
	int size = dim*32;
	int nb = (int)(density*size + .5);
	for (int i = 0; i < nb; ++i) {
	    int b = rand.nextInt(size); // uniformly turn on the bits
	    fp[b/32] |= 1<<(b%32);
	}
	return fp;
    }

    public static void main (String[] argv) throws Exception {
	int dim = 32, size = 4000000;
	SearchService sss = new SearchService (2, dim);
	Random rand = new Random ();
	for (int i = 0; i < size; ++i) {
	    int[] fp = randomFp (rand, dim);
	    sss.add("key"+i, fp);

	    if ((i+1) % 10001 == 0) {
		System.out.print("Query: "+i+"["+fp[0]);
		for (int j = 1; j< fp.length; ++j) {
		    System.out.print(","+fp[j]);
		}
		System.out.println("]");
		double den = 0.;
		for (int j = 0; j < fp.length; ++j) {
		    den += Integer.bitCount(fp[j]);
		}
		den /= (32.*dim);
		System.out.println
		    ("Query density: "+String.format("%1$.3f", den));
		
		long start = System.currentTimeMillis();
		List r = sss.filter(fp);
		double time = 1e-3*(System.currentTimeMillis()-start);
		System.out.println("** screen took "
				   +String.format("%1$.3fs",time)
				   +"; "+r.size()+" found!");
	    }
	}
	sss.shutdown();
    }
}
