/*
			 PUBLIC DOMAIN NOTICE
		     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/

package gov.nih.ncgc.search;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.logging.*;
import java.sql.*;
import javax.sql.DataSource;

import chemaxon.struc.Molecule;
import chemaxon.util.MolHandler;

import net.sf.ehcache.Element;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

// connection pooling
import com.jolbox.bonecp.*;


/**
 * This is an example of SearchService that uses a JDBC connection
 * to an Oracle database to get the fingerprints and structures.
 */
public class BARDJdbcSearchService extends SearchService {
    static final Logger logger = 
	Logger.getLogger(BARDJdbcSearchService.class.getName());

    private final static Cache cache;
    static {
	int max = Integer.getInteger("max-cache-size", 100000);
	cache = new Cache (BARDJdbcSearchService.class.getSimpleName()
			   +"-MoleculeCache", max, false, false, 60, 60);
	CacheManager.getInstance().addCache(cache);
    }

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
	catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    static final String SQLGetMol=
	"select molfile_mol from compound_molfile where cid = ?";

    class CompoundLoader implements Callable<Long> {
        CountDownLatch signal;
        long start, end;

        CompoundLoader (CountDownLatch signal, long start, long end) {
            this.start = start;
            this.end = end;
            this.signal = signal;
        }

        public Long call () {
            String thread = Thread.currentThread().getName();
            logger.info(thread+": start="+start+" end="+end);

            Connection con = null;
            long count = 0;
            try {
                con = pool.getConnection();
                Statement stm = con.createStatement
                    (ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                stm.setFetchSize(Integer.MIN_VALUE);
                ResultSet rset = stm.executeQuery
                    ("select * from compound_fp where cid between "+start
                     +" and "+end);
                long time = System.currentTimeMillis();
                long last = time;
                while (rset.next()) {
                    String cid = rset.getString("cid");
                    int[] fp = new int[getDim ()];
                    for (int i = 0; i < fp.length; ++i) {
                        fp[i] = (int)rset.getLong("fp"+i);
                    }

                    BARDJdbcSearchService.this.add(cid, fp);
                    ++count;

                    long t = System.currentTimeMillis();
                    if (t - last > 15000) {
                        logger.info(thread+": "+count+"/"
                                    +(end-start)+" CID="+cid);
                        last = t;
                    }
                }
                rset.close();
                stm.close();

                logger.info("##### "+thread+": "+count+" loaded in "
                            +String.format("%1$.3fs", 
                                           1e-3*(System.currentTimeMillis()
                                                 -time)));
            }
            catch (Exception ex) {
                logger.log(Level.SEVERE, thread+": "+ex.getMessage(), ex);
            }
            finally {
                if (con != null) {
                    try {
                        con.close();
                    }
                    catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

                signal.countDown();
            }
            return count;
        }
    }

    private BoneCP pool;
    private int nloaders = 8; // number of concurrent loaders

    public BARDJdbcSearchService () {
    }

    public BARDJdbcSearchService (int nthreads) {
	super (nthreads);
    }

    public BARDJdbcSearchService (int nthreads, int maxdepth) {
	super (nthreads, DEFAULT_FP_SIZE, maxdepth);
    }

    public void setLoaderCount (int nloaders) {
        this.nloaders = Math.max(1, nloaders);
    }
    public int getLoaderCount () { return nloaders; }

    Connection getConnection () throws Exception {
	if (pool == null) {
	    throw new IllegalArgumentException ("No Jdbc url specified!");
	}
	return pool.getConnection();
    }


    @Override
    public void load (String dbUrl) throws Exception {
	BoneCPConfig config = new BoneCPConfig ();
	config.setJdbcUrl(dbUrl);
	pool = new BoneCP (config);
	
        Connection con = null;
        try {
            con = pool.getConnection();
            Statement stm = con.createStatement();
            ResultSet rset = stm.executeQuery
                ("select max(cid) from compound_fp");
            if (rset.next()) {
                long cid = rset.getLong(1);

                long payload = cid / nloaders;
                logger.info("Max CID="+cid+" => "
                            +nloaders+" loader(s) each with payload "+payload);

                CountDownLatch signal = new CountDownLatch (nloaders);
                ExecutorService es = Executors.newCachedThreadPool();
                List<Future<Long>> loaders = new ArrayList<Future<Long>>();

                long start = 0, time = System.currentTimeMillis();
                for (int i = 0; i < nloaders; ++i) {
                    Future<Long> t = es.submit
                        (new CompoundLoader (signal, start+1, start+payload));
                    start += payload;
                    loaders.add(t);
                }
                
                signal.await(); // sit and wait
                long total = 0;
                for (Future<Long> t : loaders) {
                    try {
                        total += t.get();
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                logger.info("Time to construct signature tree of size "
                            +total+" is "
                            +String.format("%1$.3fs", 
                                           1e-3*(System.currentTimeMillis()-time)));
            }
            else {
                logger.warning("No compounds loaded!");
            }
            rset.close();
            stm.close();
        }
        finally {
            if (con != null) {
                try {
                    con.close();
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    protected Molecule getMol (Object key) {
	Element elm = cache.get(key);
	if (elm != null) {
	    return (Molecule)elm.getObjectValue();
	}

	// load into cache...
	Molecule mol = null;
	if (pool == null) {
	    return mol;
	}

	try {
	    Connection con = pool.getConnection();
	    PreparedStatement pstm = con.prepareStatement(SQLGetMol);
	    pstm.setString(1, (String)key);
	    ResultSet rset = pstm.executeQuery();
	    if (rset.next()) {
		MolHandler mh = new MolHandler (rset.getString("molfile_mol"));
		mh.aromatize();
		mol = mh.getMolecule();
		mol.hydrogenize(false);
		cache.put(new Element (key, mol));
	    }
	    rset.close();
	    pstm.close();
	    con.close();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't retrieve mol for "+key, ex);
	}

	return mol;
    }

    @Override
    public void shutdown () {
	if (pool != null) {
	    pool.shutdown();
	}
	CacheManager.getInstance().shutdown();
	super.shutdown();
    }
}
