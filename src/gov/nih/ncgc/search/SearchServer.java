/*
			 PUBLIC DOMAIN NOTICE
		     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/

package gov.nih.ncgc.search;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.zip.*;
import java.util.concurrent.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.security.SecureRandom;

import java.awt.Color;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.HandlerContainer;
import org.mortbay.jetty.handler.ContextHandlerCollection;
import org.mortbay.jetty.handler.DefaultHandler;
import org.mortbay.jetty.handler.ContextHandler;
import org.mortbay.jetty.handler.ResourceHandler;
import org.mortbay.jetty.security.Constraint;
import org.mortbay.jetty.security.ConstraintMapping;
import org.mortbay.jetty.security.HashUserRealm;
import org.mortbay.jetty.security.SecurityHandler;
import org.mortbay.jetty.security.UserRealm;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.mortbay.jetty.servlet.FilterHolder;
import org.mortbay.jetty.servlet.ServletHandler;
import org.mortbay.servlet.MultiPartFilter;
import org.mortbay.component.LifeCycle;
import org.mortbay.resource.Resource;
import org.mortbay.thread.QueuedThreadPool;


import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletResponseWrapper;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.*;
import org.apache.commons.fileupload.*;

import chemaxon.util.MolHandler;
import chemaxon.formats.MolImporter;
import chemaxon.struc.Molecule;

import gov.nih.ncgc.util.MolRenderer;


public class SearchServer implements LifeCycle.Listener {
    static private final Logger logger = 
	Logger.getLogger(SearchServer.class.getName());

    static final String VERSION = "v0.0.1";
    static final String REALM_FILE = "realm.properties";

    static final String REALM_ROLE_USER = "user";
    static final String REALM_ROLE_ADMIN = "admin";
    static final String REALM_ROLE_GUEST = "guest";
    static final String[] COLORS = new String[]{"#f1f5fa","#ffffff"};

    static final Color[] BACKGROUND_COLORS = new Color[]{
	new Color (0xf1,0xf5,0xfa), Color.white
    };

    /*
     * The base context
     */
    static final String CONTEXT_BASE = 
	System.getProperty("context-base", "/");

    public static final String CONTEXT_LOAD = "/load";
    public static final String CONTEXT_STATUS = "/status";
    public static final String CONTEXT_SEARCH = "/search";
    public static final String CONTEXT_ADMIN = "/admin";
    public static final String CONTEXT_COUNT = "/count";
    static final String CONTEXT_WORK = "/work";


    static final String PATH_WORK = "work"; // work path
    static final String PATH_HTML = "html"; // work path
    static final int DEFAULT_IMAGE_SIZE = 150;
    // no more than this many structures at any given of time
    static final int MAX_BATCH_SIZE = 100; 

    public static final String PUBCHEM_CID_URL = 
	"http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?cid=";

    static final String HTML_HEAD;
    static final String HTML_TAIL;
    static {
	StringBuilder head = new StringBuilder ();
	StringBuilder tail = new StringBuilder ();
	try {
	    BufferedReader br = new BufferedReader
		(new InputStreamReader (SearchServer.class.getResourceAsStream
					("resources/template.html")));
	    boolean content = false;
	    for (String line; (line = br.readLine()) != null; ) {
		if ("[CONTENT]".equals(line)) {
		    content = true;
		}
		else if (!content) {
		    head.append(line+"\n");
		}
		else {
		    tail.append(line+"\n");
		}
	    }
	    br.close();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Html template doesn't exist!", ex);
	}
	HTML_HEAD = head.toString();
	HTML_TAIL = tail.toString();
    }

    static final HashUserRealm USER_REALM;
    static {
	URL url = SearchServer.class.getResource("resources/"+REALM_FILE);
	HashUserRealm realm = new HashUserRealm 
	    ("SearchServer: Admin requires authentication");
	try {
	    realm.setConfig(url.toString());
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't load user realm: "+url, ex);
	}
	USER_REALM = realm;
    }

    class FilterPrintWriter extends PrintWriter {
	FilterPrintWriter (PrintWriter writer) {
	    super (writer);
	}

	@Override
	public void write (char[] buf, int off, int len) {
	    super.write(buf, off, len);
	    System.out.print(new String (buf, off, len));
	}
	public void print (String s) {
	    super.print(s);
	    System.out.print(s);
	}
	public void println (String s) {
	    super.println(s);
	    System.out.println(s);
	}
    }

    class HtmlFilterResponse extends HttpServletResponseWrapper {
	HtmlFilterResponse (HttpServletResponse res) {
	    super (res);
	}
	
	@Override
	public PrintWriter getWriter () throws IOException {
	    logger.info("HtmlFilterResponse invoked");
	    return new FilterPrintWriter (super.getWriter());
	}
    }

    class HtmlFilter implements Filter {
	public void init (FilterConfig config) throws ServletException {
	}

	public void destroy () {
	}

	public void doFilter (ServletRequest request, 
			      ServletResponse response, 
			      FilterChain chain) 
	    throws IOException, ServletException {
	    HttpServletRequest req = (HttpServletRequest)request;
	    HttpServletResponse res = (HttpServletResponse)response;
	    chain.doFilter(req, new HtmlFilterResponse (res));
	}
    }

    class HtmlProcessor {
	PrintWriter pw;
	String context;

	HtmlProcessor (PrintWriter pw) {
	    this.pw = pw;

	    context = getBaseContext ();
	    if ("/".equals(context)) {
		context = "";
	    }

	    for (String line : HTML_HEAD.split("\n")) {
		for (int pos; (pos = line.indexOf("[CONTEXT]")) >= 0; ) {
		    pw.print(line.substring(0, pos));
		    pw.print(context);
		    line = line.substring(pos+9);
		}
		pw.println(line);
	    }
	    pw.flush();
	}

	HtmlProcessor p (String s) {
	    pw.println(s);
	    return this;
	}

	void end () {
	    end (null);
	}

	void end (String status) {
	    if (status != null) {
		StringBuilder sb = new StringBuilder 
		    ("<div class=\"block\">"
		     +"<h3 class=\"widgettitle\">Status</h3>");
		sb.append(status);
		sb.append("</div>");
		status = sb.toString();
	    }
	    else {
		status = "";
	    }
	    for (String line : HTML_TAIL.split("\n")) {
		if ("[STATUS]".equals(line)) {
		    pw.println(status);
		}
		else {
		    pw.println(line);
		}
	    }
	    pw.flush();
	}

	HtmlProcessor p (Throwable t) {
	    t.printStackTrace(pw);
	    return this;
	}

	HtmlProcessor flush () {
	    pw.flush();
	    return this;
	}
    }

    class Loader implements Callable<Integer> {
	Object source;
	String error;

	Loader (File file) {
	    source = file;
	}

	Loader (URL url) {
	    source = url;
	}
	
	public Integer call () throws Exception {
	    int count = 0;
	    try {
		MolImporter mi = new MolImporter (getInputStream ());
		
		for (Molecule m; (m = mi.read()) != null; ++count) {
		    m.hydrogenize(false);
		    m.aromatize();
		    service.add(m);
		}
		mi.close();
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't parse input "+source, ex);
		error = ex.getMessage();
	    }
	    return count;
	}

	InputStream getInputStream () throws IOException {
	    if (source instanceof File) {
		try {
		    return new GZIPInputStream 
			(new FileInputStream ((File)source));
		}
		catch (IOException ex) {
		    return new FileInputStream ((File)source);
		}
	    }
	    else {
		try {
		    return new GZIPInputStream (((URL)source).openStream());
		}
		catch (IOException ex) {
		    return ((URL)source).openStream();
		}
	    }
	}

	String getError () { return error; }
    }

    class LoadTask extends FutureTask<Integer> {
	String name;
	Loader loader;

	LoadTask (String name, Loader loader) {
	    super (loader);
	    this.name = name;
	    this.loader = loader;
	}

	public String getError () { return loader.getError(); }
	public String getName () { return name; }
    }


    class SearchProcessorHtml implements SearchCallback<Molecule> {
	File path;
	String context;
	int columns; // number of columns
	int index = 0;
	int count = 0;
	MolRenderer renderer = new MolRenderer ();
	HtmlProcessor html;

	SearchProcessorHtml (HtmlProcessor html, String query, int index, 
			     int columns) throws IOException {
	    String hex = Integer.toHexString(query.hashCode());

	    path = new File (PATH_WORK, hex);
	    path.mkdirs();
	    context = getWorkContext()+"/"+hex+"/";
	    this.columns = columns;
	    this.index = index;
	    this.html = html;
	}

	int getCount () { return count; }

	String createImage (String name, Molecule mol) {
	    File img = new File (path, name);
	    if (!img.exists()) {
		try {
		    renderer.setBackground(Color.white);
		    ImageIO.write(renderer.createImage
				  (mol, DEFAULT_IMAGE_SIZE), "png", img);
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, 
			       "Can't generate image "+name, ex);
		    return null;
		}
	    }
	    return "<img src=\""+context+name+"\" alt=\""+name+"\">";
	}

	public boolean matched (Molecule mol) {
	    String name = mol.getName();
	    if (name == null || name.equals("")) {
		// use the index count
		name = String.valueOf(index);
	    }

	    int bg = (index + (index/columns & 1)) % BACKGROUND_COLORS.length;
	    String image = name+"-"+bg+".png";
	    File file = new File (path, image);

	    if (index % columns == 0) { // new row
		if (index != 0) {
		    html.p("</tr>");
		}
		html.p("<tr>");
	    }

	    if (!file.exists()) {
		// generate an image
		try {
		    renderer.setBackground(BACKGROUND_COLORS[bg]);
		    ImageIO.write(renderer.createImage
				  (mol, DEFAULT_IMAGE_SIZE), "png", file);
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, 
			       "Can't generate image for "+name, ex);
		}
	    }
	    html.p("<td><img src=\""+context+image
		   +"\" alt=\""+name+"\"><br><center>");
	    String link = getLinkOutUrl ();
	    if (link != null) {
		html.p("<a href=\""+link+name+"\">"+name+"</a>");
	    }
	    else {
		html.p(name);
	    }
	    html.p("</center></td>");
	    html.flush();

	    ++index;
	    ++count;

	    return true;
	}
    }

    class SearchProcessorText implements SearchCallback<Molecule> {
	PrintWriter pw;
	int limits, index;
	String format;
	boolean smiles;

	SearchProcessorText (PrintWriter pw, String format, 
			     int index, int limits) {
	    this.pw = pw;
	    this.limits = limits;
	    this.format = format;
	    smiles = format.startsWith("smiles") 
		|| format.startsWith("smarts")
		|| format.startsWith("cxsmiles") 
		|| format.startsWith("cxsmarts");
	}

	synchronized public boolean matched (Molecule mol) {
	    try {
		pw.print(mol.toFormat(format));
		if (smiles) {
		    pw.println("\t"+mol.getName());
		}

		return ++index < limits || limits <= 0;
	    }
	    catch (Exception ex) {
		// bogus format?
		logger.log(Level.SEVERE, "Bogus format? "+format, ex);
	    }
	    return false; // stop now
	}

	int getIndex () { return index; }
    }

    class Search extends HttpServlet {

	@Override
	protected void service (HttpServletRequest req, 
				HttpServletResponse res)
	    throws IOException, ServletException {

	    if (disableContexts.contains(req.getServletPath())) {
		res.setContentType("text/html");
		HtmlProcessor html = new HtmlProcessor (res.getWriter());
		html.p("<h3>Sorry, this feature has been "
		       +"disabled for security reasons!</h3>");
		html.end();
	    }
	    else {
		super.service(req, res);
	    }
	}
	    
	@Override
	protected void doGet (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
	    String mode = req.getParameter("mode");
	    if (mode != null && mode.equals("text")) {
		doGetText (req, res);
	    }
	    else {
		doGetHtml (req, res);
	    }
	}

	@Override
	protected void doPost (HttpServletRequest req, 
			       HttpServletResponse res) 
	    throws IOException, ServletException {
	    // currently only allow post via html
	    doPostHtml (req, res);
	}

	void doGetText (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
	    String s = req.getParameter("s");
	    if (s == null) {
		res.sendError(HttpServletResponse.SC_BAD_REQUEST,
			      "No query specified!");
		return;
	    }

	    String format = req.getParameter("format");
	    if (format == null) {
		format = "sdf";
	    }
            
            String type = req.getParameter("type"); // exact?

	    String max = req.getParameter("max");
	    int limits = 1000;
	    if (max != null) {
		try {
		    // hard limit
		    limits = Math.min(10000, Integer.parseInt(max));
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, "Bogus max value: "+max
			       +"; reset to default value!", ex);
		}
	    }

            if (req.getParameter("_nolimits") != null) {
                logger.info("### No limits on results!");
                limits = 0;
            }

	    res.setContentType("text/plain");
	    try {
		Molecule query = new MolHandler(s, true).getMolecule();
		query.aromatize();
		long start = System.currentTimeMillis();
		SearchProcessorText sp = new SearchProcessorText
		    (res.getWriter(), format, 0, limits);
		int rows = service.search
                    (query, limits, type != null 
                     && type.equalsIgnoreCase("exact"), sp);
                logger.info("#### Query "+query.toFormat("cxsmarts")
                            +" => "+rows);
	    }
	    catch (Exception ex) {
		res.sendError
		    (HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
	    }
	}

	void doGetHtml (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
            res.setContentType("text/html");
	    HtmlProcessor html = new HtmlProcessor (res.getWriter());

	    String s = req.getParameter("s");
	    if (s == null) {
		html.p("<h3>No query specified!</h3>");
		html.end();
		return;
	    }

	    String status = null;
	    try {
		Molecule query = new MolHandler(s, true).getMolecule();
		query.aromatize();

		html.p("<center>");
		html.p("<table width=\"80%\" cellpadding=\"0\" "
		       +"cellspacing=\"0\" border=\"0\">");
		
		// only allow max results returned
		int columns = 4, limit = 100; // limit x nthreads/indexes
		SearchProcessorHtml sp = new SearchProcessorHtml 
		    (html, s, 0, columns);

		long start = System.currentTimeMillis();
		int rows = service.search(query, limit, sp);
		double time = (System.currentTimeMillis()-start)*1e-3;

		if (rows >= limit) {
		    // get a bit more accurate count here
		    rows = service.count(query);
		}

		if (sp.getCount() % columns != 0) {
		    html.p("</tr>");
		}

		html.p("</table>");
		html.p("</center>");

		String showQuery = sp.createImage("query.png", query);
		if (showQuery == null) {
		    showQuery = "<font color=red><code>"+s
			+"</code></font>";
		}
		
		status = ("<b>Query "+showQuery+" returns <font color=blue>"
			  +sp.getCount()+" matches</font> in " 
			  +String.format("%1$.3fs", time)+"</b>");
		if (sp.getCount() >= limit && rows > sp.getCount()) {
		    status += ("; there are potentially "+(rows-sp.getCount())
			       +" more matches!");
		}
	    }
	    catch (Exception ex) {
		html.p("<h3>Error: <font color=red>"
		       +ex.getMessage()+"</font></h3>");
		html.p("<code>").p(ex).p("</code>");
	    }
	    html.end(status);
	}


	void doPostHtml (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
            res.setContentType("text/html");
	    HtmlProcessor html = new HtmlProcessor (res.getWriter());

	    String q = req.getParameter("q");
	    if (q == null) {
		html.p("<h3>No query specified!</h3>");
		html.end();
		return;
	    }

	    String status = null;
	    try {
		html.p("<center>");
		html.p("<table width=\"80%\" cellpadding=\"0\" "
		       +"cellspacing=\"0\" border=\"0\">");
		MolImporter mi = new MolImporter 
		    (new ByteArrayInputStream (q.getBytes("utf-8")));
		
		int r = 0, total = 0, cnt = 0;
		int columns = 4, limit = 100; // per query per thread
		int max = limit;
		long start = System.currentTimeMillis();

		SearchProcessorHtml sp = null;
		Molecule query = new Molecule ();
		for (;  mi.read(query) && cnt < MAX_BATCH_SIZE; ++cnt) {
		    String name = query.toFormat("cxsmarts");
		    sp = new SearchProcessorHtml (html, name, r, columns);
		    int rows = service.search(query, limit, sp);
		    r += sp.getCount();
		    total += rows;
		    html.flush();
		    limit = Math.max(10, limit/2);
		}
		if (total % columns != 0) {
		    html.p("</tr>");
		}

		double time = (System.currentTimeMillis()-start)*1e-3;
		html.p("</table>").p("</center>");

		if (cnt == 1) {
		    String showQuery = sp.createImage("query.png", query);
		    if (showQuery == null) {
			showQuery = "<font color=red><code>"
			    +query.toFormat("cxsmarts")+"</code></font>";
		    }

		    if (total >= max) {
			// get a bit more accurate count here
			total = service.count(query);
		    }

		    status = ("<b>Query "+showQuery
			      +" returns <font color=blue>"
			      +sp.getCount()+" matches</font> in " 
			      +String.format("%1$.3fs", time)+"</b>");
		    if (r >= max && total > r) {
			status += ("; there are potentially "
				   +(total-r)  +" more matches!");
		    }
		}
		else {
		    status = ("<b>Query results for <font color=red><code>"+
			      cnt+"</code></font> structures "
			      +"return <font color=blue>"
			      +r+" matches</font> out of potentially "
			      +total+" matches; total time = " 
			      +String.format("%1$.3fs", time)+"</b>");
		}
	    }
	    catch (Exception ex) {
		html.p("<h3>Error: <font color=red>"
		       +ex.getMessage()+"</font></h3>");
		html.p("<code>").p(ex).p("</code>");
	    }
	    html.end(status);
	}
    }

    class Status extends HttpServlet {
	@Override
	protected void doGet (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
	    HtmlProcessor html = new HtmlProcessor (res.getWriter());
	    res.setContentType("text/html");

	    String status = null;
	    if (disableContexts.contains(req.getServletPath())) {
		html.p("<h3>Sorry, this feature has been disabled for "
		       +"security reasons!</h3>");
	    }
	    else {
		int index = 0, total = 0;
		for (SignatureIndex.IndexStats stats : service.getStats()) {
		    html.p("<br>").p("<h3>Index "+(index+1)+"</h3>");
		    total += showStats (html, stats);
		    html.p("<br>");
		    ++index;
		}
		if (index == 0) {
		    html.p("<h3>No index statistics available; "
			   +"perhaps no data loaded yet!</h3>");
		}
		else {
		    html.p("<i><font size=\"-2\">Generated on "
			   +new java.util.Date()+"</font></i>");
		    status = "<font color=blue><b>Total entries indexed: "
			+total+"</b></font>";

		}
	    }
	    html.end(status);
	}

	int showStats (HtmlProcessor html, 
		       SignatureIndex.IndexStats stats) throws IOException {
	    html.p("<table width=\"90%\" cellpadding=\"5\" "
		   +"cellspacing=\"0\" border=\"0\">");
	    html.p("<tr>");
	    html.p("  <th>Property</th>");
	    html.p("  <th>&nbsp;&nbsp;</th>");
	    html.p("  <th>Value</th>");
	    html.p("</tr>");
	    
	    int row = 0;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">NumNodes</td><td></td>"
		   +"<td align=\"left\">"+stats.nodeCount()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">NumLeafs</td><td></td>"
		   +"<td align=\"left\">"+stats.leafCount()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">MaxDepth</td><td></td>"
		   +"<td align=\"left\">"+stats.maxDepth()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">AvgLeafSize</td><td></td>"
		   +"<td align=\"left\">"+String.format
		   ("%1$.3f", stats.avgLeafSize())+"</td>");
	    html.p("</tr>");
	    
	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">MinLeafSize</td><td></td>"
		   +"<td align=\"left\">"+stats.minLeafSize()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">MaxLeafSize</td><td></td>"
		   +"<td align=\"left\">"+stats.maxLeafSize()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">Size</td><td></td>"
		   +"<td align=\"left\">"+stats.size()+"</td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">MinSig</td><td></td>");
	    int[] sig = stats.minSignature();
	    html.p("<td align=\"left\"><code>");
	    for (int i = 0; i < sig.length; ++i) {
		html.p("("+(sig[i]>>1)+","+(sig[i]&1)+") ");
	    }
	    html.p("</code></td>");
	    html.p("</tr>");

	    ++row;
	    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
	    html.p(" <td align=\"right\">MaxSig</td><td></td>");
	    sig = stats.maxSignature();
	    html.p("<td align=\"left\"><code>");
	    for (int i = 0; i < sig.length; ++i) {
		html.p("("+(sig[i]>>1)+","+(sig[i]&1)+") ");
	    }
	    html.p("</code></td>");
	    html.p("</tr>");
	    html.p("</table>");

	    return stats.size();
	}
    }

    class Load extends HttpServlet {
	SecureRandom rand = new SecureRandom ();
	ConcurrentMap<String, LoadTask> tasks = 
	    new ConcurrentHashMap<String, LoadTask>();
	
	synchronized String getRequestId () {
	    byte[] buf = new byte[8];
	    rand.nextBytes(buf);
	    StringBuilder sb = new StringBuilder ();
	    for (int i = 0; i < buf.length; ++i) {
		sb.append(String.format("%1$02x", buf[i]));
	    }
	    return sb.toString();
	}

	String getStatus (LoadTask task) 
	    throws ServletException {
	    if (task.isDone()) {
		try {
		    StringBuilder sb = new StringBuilder ("Finished");
		    if (task.getError() != null) {
			sb.append("; "+task.get() 
				  +" structures processed "
				  +"(<font color=\"red\">"
				  +task.getError()+"</font>)");
		    }
		    else {
			sb.append("; "+task.get()+" structures processed!");
		    }
		    return sb.toString();
		}
		catch (Exception ex) {
		    throw new ServletException (ex);
		}
	    }
	    else if (task.isCancelled()) {
		return "Canceled" + (task.getError() != null 
				     ? (" "+task.getError()) : "");
	    }
	    else {
		return "Running";
	    }
	}

	@Override
	protected void doGet (HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, IOException {
	    HtmlProcessor html = new HtmlProcessor (res.getWriter());
	    res.setContentType("text/html");

	    if (disableContexts.contains(req.getServletPath())) {
		html.p("<h3>Sorry, this feature has been disabled for "
		       +"security reasons!</h3>");
	    }
	    else if (tasks.isEmpty()) {
		html.p("<h3>Task queue is empty!</h3>");
	    }
	    else {
		html.p("<table width=\"80%\" cellpadding=\"5\" "
		       +"cellspacing=\"0\" border=\"0\">");
		html.p("<tr>");
		html.p("  <th align=\"left\">Source</th>");
		html.p("  <th align=\"center\">Status</th>");
		html.p("</tr>");
		
		int row = 0;
		for (LoadTask task : tasks.values()) {
		    html.p("<tr bgcolor=\""+COLORS[row%2]+"\">");
		    html.p(" <td align=\"left\"><code>"
			   +task.getName()+"</code></td>");
		    html.p(" <td align=\"left\">"+getStatus(task)+"</td>");
		    html.p("</tr>");
		    ++row;
		}
		html.p("</table>");
		html.p("<br>");
		html.p("<br>");
		html.p("<i><font size=\"-2\">Generated on "
		       +new java.util.Date()+"</font></i>");
	    }
	    html.end();
	}

	@Override
	protected void doPost (HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, IOException {
	    res.setContentType("text/html");

	    String url = req.getParameter("url");
	    HtmlProcessor html;

	    if (disableContexts.contains(req.getServletPath())) {
		html = new HtmlProcessor (res.getWriter());
		html.p("<h3>Sorry, this feature has been disabled for "
		       +"security reasons!</h3>");
		html.end();
	    }
	    else {
		try {
		    if (url != null) {
			URL u = new URL (url);
			logger.info("URL="+u);
			url = u.toString();
			LoadTask task = new LoadTask (url, new Loader (u));
			threadPool.submit(task);
			tasks.put(url, task);
		    }
		    else {
			boolean multipart = 
			    ServletFileUpload.isMultipartContent(req);
			//logger.info("Multipart request: " + multipart);
			if (multipart) {
			    parseMultiPart (req, res);
			}
			/*
			else {
			    html.p("<h3>Error: Not POST is not "
				   +"multipart encoding!</h3>");
			}
			*/
		    }
		    
		    res.sendRedirect(req.getServletPath());
		}
		catch (Exception ex) {
		    html = new HtmlProcessor (res.getWriter());
		    //res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    html.p("<h3>"+ex.getMessage()+"</h3>");
		    html.p("<code>").p(ex).p("</code>");
		    html.end();
		}
	    }
	}

	void parseMultiPart (HttpServletRequest req, HttpServletResponse res) 
	    throws Exception {
	    // Create a new file upload handler
	    ServletFileUpload upload = new ServletFileUpload();

	    // Parse the request
	    FileItemIterator iter = upload.getItemIterator(req);
	    int files = 0;
	    while (iter.hasNext()) {
		FileItemStream item = iter.next();
		String field = item.getFieldName();
		InputStream stream = item.openStream();
		if (item.isFormField()) {
		    String value = Streams.asString(stream);
		    logger.info
			("Form field \"" + field + "\" with value \""
			 + value + "\" detected.");
		}
		else if (!tasks.containsKey(item.getName())) {
		    File file = createTempFile (item);
		    if (file.length() > 0) {
			LoadTask task = new LoadTask 
			    (item.getName(), new Loader (file));
			threadPool.submit(task);
			tasks.put(item.getName(), task);
			++files;
		    }
		    else {
			logger.warning("Ignore empty file: "
				       +item.getName());
		    }
		    /*
		      logger.info
		      ("File field " + field + " with file name "
		      + item.getName() + " detected => "+file);
		    */
		}
	    }
	}


	File createTempFile (FileItemStream item) throws IOException {
	    String name = item.getName().replaceAll("\\s", "_");
	    int pos = name.lastIndexOf('/');
	    if (pos > 0) {
		name = name.substring(pos+1);
	    }
	    pos = name.lastIndexOf('\\');
	    if (pos > 0) {
		name = name.substring(pos+1);
	    }
	    
	    File file = File.createTempFile("xyz", "_"+name+".tmp");
	    FileOutputStream fos = new FileOutputStream (file);
	    InputStream is = item.openStream();
	    byte[] buf = new byte[1024];
	    for (int nb; (nb = is.read(buf, 0, buf.length)) > 0; ) {
		fos.write(buf, 0, nb);
	    }
	    fos.close();
	    return file;
	}
    }

    class Admin extends HttpServlet {
	@Override
	protected void doGet
	    (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
            res.setContentType("text/html");
	    HtmlProcessor html = new HtmlProcessor (res.getWriter());
	    String action = req.getParameter("a");

	    /*
	    logger.info("PathInfo=\""+req.getPathInfo()+"\"");
	    logger.info("ServetPath=\""+req.getServletPath()+"\"");
	    logger.info("ContextPath=\""+req.getContextPath()+"\"");
	    */

	    if (disableContexts.contains(req.getServletPath())) {
		html.p("<h3>Sorry, this feature has been disabled for "
		       +"security reasons!</h3>");
	    }
	    else {
		if (action == null) {
		    html.p("<h3>Available actions</h3>");
		    html.p("<ul>");
		    html.p(" <li><a href=\"/admin?a=shutdown\">"
			   +"Shutdown server</a></li>");
		    html.p("</ul>");
		}
		else if ("shutdown".equalsIgnoreCase(action)) {
		    stop ();
		}
		else {
		    html.p("Unknown action: <code>"+action+"</code>");
		}
	    }
	    html.end();
	}
    }

    class Count extends HttpServlet {
	@Override
	protected void doGet (HttpServletRequest req, HttpServletResponse res) 
	    throws IOException, ServletException {
	    PrintWriter pw = res.getWriter();
	    res.setContentType("text/plain");
	    String s = req.getParameter("s");
	    if (s == null) {
		res.sendError(HttpServletResponse.SC_BAD_REQUEST,
			      "No query specified!");
	    }
	    try {
		Molecule query = new MolHandler(s, true).getMolecule();
		query.aromatize();
		long start = System.currentTimeMillis();
		int count = service.count(query);
		double time = (System.currentTimeMillis() - start)*1.e-3;
		pw.println(s+"\t"+count+"\t"+String.format("%1$.3fs", time));
	    }
	    catch (Exception ex) {
		res.sendError
		    (HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
	    }
	}
    }


    private Server server;
    private ExecutorService threadPool = Executors.newFixedThreadPool(100);
    private volatile SearchService service;
    private String linkOutUrl = null;
    private String baseContext = CONTEXT_BASE;
    private Set<String> disableContexts = new HashSet<String>();

    public SearchServer (SearchService service) {
	this (8080, CONTEXT_BASE, service);
    }

    public SearchServer (int port, String context, SearchService service) {
	if (service == null) {
	    throw new IllegalArgumentException ("SearchService can't be null");
	}

	this.service = service;
	this.baseContext = context;
	server = new Server (port);
        server.setSendServerVersion(false);
        server.setStopAtShutdown(true);
        server.setGracefulShutdown(100);
        server.setThreadPool(new QueuedThreadPool (100));
	//server.addLifeCycleListener(this);
	server.addUserRealm(USER_REALM);
    }

    /**
     * LifeCycle.Listener interface
     */
    public void lifeCycleFailure (LifeCycle ev, Throwable ex) {
	logger.info("## "+ev);
    }
    public void lifeCycleStarted (LifeCycle ev) {
	logger.info("## "+ev);
    }
    public void lifeCycleStarting (LifeCycle ev) {
	logger.info("## "+ev);
    }
    public void lifeCycleStopped (LifeCycle ev) {
	logger.info("## "+ev);
    }
    public void lifeCycleStopping (LifeCycle ev) {
	logger.info("## "+ev);
    }

    public String getBaseContext () { return baseContext; }
    /*
     * Note, this method must be called before start()!
     */
    public void setBaseContext (String context) {
	this.baseContext = context;
    }
    protected String getWorkContext () {
	String ctx = getBaseContext ();
	if ("/".equals(ctx)) {
	    ctx = "";
	}
	return ctx + CONTEXT_WORK;
    }

    public void start () {
	try {
	    createServerContexts (server);

	    server.start();
	    server.join();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't start server", ex);
	}
    }

    public void stop () {
	logger.info("** Stopping server");
	try {
	    server.stop();
	    service.shutdown();
	    threadPool.shutdownNow();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't stop server", ex);
	}
    }

    /*
     * context argument is one of the CONTEXT_* values; this method must
     * be called before start()!
     */
    public void enableContext (String context, boolean enable) {
	if (enable) disableContexts.remove(context);
	else disableContexts.add(context);
    }

    public void setLinkOutUrl (String url) { this.linkOutUrl = url; }
    public String getLinkOutUrl () { return linkOutUrl; }


    protected void createServerContexts (HandlerContainer container) 
	throws IOException {

	ServletHandler sh = new ServletHandler ();
	sh.addServletWithMapping(new ServletHolder (new Load ()), 
				 CONTEXT_LOAD);
	sh.addServletWithMapping(new ServletHolder (new Status ()), 
				 CONTEXT_STATUS);
	sh.addServletWithMapping(new ServletHolder (new Search ()), 
				 CONTEXT_SEARCH);
	sh.addServletWithMapping(new ServletHolder (new Count ()), 
				 CONTEXT_COUNT);
	ServletHolder serv = new ServletHolder (new Admin ());
	serv.setRunAs(REALM_ROLE_ADMIN);
	sh.addServletWithMapping(serv, CONTEXT_ADMIN);

        ContextHandlerCollection contexts = new ContextHandlerCollection();

	ContextHandler htmlHandler = new ContextHandler
	    (contexts, getBaseContext ());
	ResourceHandler htmlContents = new ResourceHandler ();
	htmlContents.setResourceBase(PATH_HTML);
	htmlContents.setWelcomeFiles(new String[]{"index.html"});
	htmlHandler.setHandler(htmlContents);

        ResourceHandler staticContents = new ResourceHandler();
	staticContents.setBaseResource
	    (Resource.newResource(getClass().getResource("resources")));
        ContextHandler staticHandler = new ContextHandler 
	    (contexts, getBaseContext ());
        staticHandler.setHandler(staticContents);


	ContextHandler dynHandler = new ContextHandler 
	    (contexts, getWorkContext ());
	ResourceHandler dynContents = new ResourceHandler ();
	dynContents.setResourceBase(PATH_WORK);
	dynHandler.setHandler(dynContents);

	Context context = new Context 
	    (contexts, getBaseContext (), Context.NO_SESSIONS);
	context.addLifeCycleListener(this);

        Constraint constraint1 = new Constraint();
	constraint1.setName(Constraint.__BASIC_AUTH);
	constraint1.setRoles(new String[]{REALM_ROLE_ADMIN});
	constraint1.setAuthenticate(true);

	Constraint constraint2 = new Constraint();
	constraint2.setName(Constraint.NONE);
	constraint2.setAuthenticate(false);
	constraint2.setRoles(new String[]{REALM_ROLE_USER,REALM_ROLE_GUEST});

        ConstraintMapping cm1 = new ConstraintMapping();
        cm1.setConstraint(constraint1);
        cm1.setPathSpec(CONTEXT_ADMIN);

	ConstraintMapping cm2 = new ConstraintMapping ();
	cm2.setConstraint(constraint2);
	cm2.setPathSpec("/*");

	SecurityHandler sec = new SecurityHandler();
	sec.setUserRealm(USER_REALM);
	sec.setConstraintMappings(new ConstraintMapping[]{cm2, cm1});
	context.setSecurityHandler(sec);
	context.setServletHandler(sh);
	/*
	context.addFilter(new FilterHolder 
			  (new HtmlFilter ()), "/*", ContextHandler.ALL);
	*/
	container.addHandler(contexts);
    }

    protected SecurityHandler createSecurityHandler
            (String path, String... roles) {
        Constraint constraint = new Constraint();
        if (roles.length > 0) {
            constraint.setName(Constraint.__BASIC_AUTH);
            constraint.setRoles(roles);
            constraint.setAuthenticate(true);
        } else {
            constraint.setName(Constraint.NONE);
            constraint.setAuthenticate(false);
        }
        ConstraintMapping cm = new ConstraintMapping();
        cm.setConstraint(constraint);
        cm.setPathSpec(path);

	SecurityHandler sh = new SecurityHandler();
	sh.setUserRealm(USER_REALM);
	sh.setConstraintMappings(new ConstraintMapping[]{cm});

        return sh;
    }

    static SearchService createJdbcService (final String url, 
                                            int nthreads, 
					    final String... argv) {
	final SearchService service = new BARDJdbcSearchService (nthreads);
	service.setScreenLimit(Integer.MAX_VALUE);
	new Thread(new Runnable () {
		public void run () {
		    // start loading from a background thread
		    try {
			service.load(url);
		    }
		    catch (Exception ex) {
			ex.printStackTrace();
		    }
		}
	    }).start();
	return service;
    }

    static SearchService createFileService (int nthreads, 
					    final String... files) {
	final SearchService service = new BasicSearchService (nthreads);
	service.setScreenLimit(Integer.MAX_VALUE);
	new Thread(new Runnable () {
		public void run () {
		    for (String f : files) {
			try {
			    service.load(f);
			}
			catch (Exception ex) {
			    logger.log(Level.SEVERE, "Can't load file "+f, ex);
			}
		    }
		}
	    }).start();
	return service;
    }


    public static void main (String[] argv) throws Exception {
	int port = 8080;
	int nthreads = 2;
	String context = "/", jdbc = null;
        boolean admin = false;

	List<String> nonoptions = new ArrayList<String>();
	for (String a : argv) {
            int pos = a.indexOf('=');
            if (pos > 0) {
                String[] toks = new String[]{
                    a.substring(0, pos),
                    a.substring(pos+1)
                };

		if (toks[0].equalsIgnoreCase("port")) {
		    try {
			port = Integer.parseInt(toks[1]);
		    }
		    catch (Exception ex) {
			logger.warning("Invalid port option: "+toks[1]);
		    }
		}
		else if (toks[0].equalsIgnoreCase("threads")) {
		    try {
			nthreads = Integer.parseInt(toks[1]);
		    }
		    catch (Exception ex) {
			logger.warning("Invalid number of threads: "+toks[1]);
		    }
		}
		else if (toks[0].equalsIgnoreCase("context")) {
		    context = toks[1];
		}
                else if (toks[0].equalsIgnoreCase("jdbc")) {
                    jdbc = toks[1];
                }
                else if (toks[0].equalsIgnoreCase("admin")) {
                    admin = Boolean.parseBoolean(toks[1]);
                }
	    }
	    else {
		nonoptions.add(a);
	    }
	}

	logger.info("Starting server: port="
		    +port+" threads="+nthreads+" context="+context
                    +" jdbc="+jdbc+" admin="+admin);
	logger.info("#### Please make sure html/index.html and "
		    +"html/load.html\nare edited accordingly "
		    +"for the CONTEXT "+context+" ####");

	SearchService service = jdbc != null ? createJdbcService 
            (jdbc, nthreads, nonoptions.toArray(new String[0]))
            : createFileService (nthreads, nonoptions.toArray(new String[0]));

	SearchServer server = new SearchServer (port, context, service);
	server.setLinkOutUrl(PUBCHEM_CID_URL);
        server.enableContext(CONTEXT_ADMIN, admin); // admin
        server.enableContext(CONTEXT_LOAD, jdbc == null); // load
	server.start();
    }
}
