/*
			 PUBLIC DOMAIN NOTICE
		     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software/database is freely
available to the public for use. The NIH Chemical Genomics Center
(NCGC) and the U.S. Government have not placed any restriction on its
use or reproduction. 

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.

*/

package gov.nih.ncgc.search;

import java.util.concurrent.*;
import java.util.logging.*;
import java.sql.*;
import javax.sql.DataSource;

import chemaxon.struc.Molecule;
import chemaxon.util.MolHandler;

import net.sf.ehcache.Element;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

// connection pooling
import com.jolbox.bonecp.*;


/**
 * This is an example of SearchService that uses a JDBC connection
 * to an Oracle database to get the fingerprints and structures.
 */
public class JdbcSearchService extends SearchService {
    static final Logger logger = 
	Logger.getLogger(JdbcSearchService.class.getName());

    static {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        }
        catch (Exception ex) {
            logger.log(Level.SEVERE, "Can't load oracle driver", ex);
        }
    }

    private final static Cache cache;
    static {
	int max = Integer.getInteger("max-cache-size", 100000);
	cache = new Cache (JdbcSearchService.class.getSimpleName()
			   +"-MoleculeCache", max, false, false, 60, 60);
	CacheManager.getInstance().addCache(cache);
    }


    static final String SQLGetFp;
    static {
	int max = Integer.getInteger("max-index-size", 1000000);
	if (max <= 0) {
	    SQLGetFp = "select * from compound_fp";
	}
	else {
	    SQLGetFp = "select * from compound_fp where rownum<="+max;
	}
    }

    static final String SQLGetMol=
	"select molfile from compound where cid = ?";

    private BoneCP pool;

    public JdbcSearchService () {
    }

    public JdbcSearchService (int nthreads) {
	super (nthreads);
    }

    public JdbcSearchService (int nthreads, int maxdepth) {
	super (nthreads, DEFAULT_FP_SIZE, maxdepth);
    }


    Connection getConnection () throws Exception {
	if (pool == null) {
	    throw new IllegalArgumentException ("No Jdbc url specified!");
	}
	return pool.getConnection();
    }


    @Override
    public void load (String dbUrl) throws Exception {
	BoneCPConfig config = new BoneCPConfig ();
	config.setJdbcUrl(dbUrl);
	pool = new BoneCP (config);
	
	Connection con = pool.getConnection();
	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery(SQLGetFp);

	logger.info("Loading fingerprint...");
	long count = 0, start = System.currentTimeMillis();
	while (rset.next()) {
	    String cid = rset.getString("cid");
	    int[] fp = new int[getDim ()];
	    for (int i = 0; i < fp.length; ++i) {
		fp[i] = (int)rset.getLong("fp"+i);
	    }
	    add (cid, fp);
	    if (++count % 100000 == 0) {
		logger.info(cid+": "+count);
	    }
	}
	rset.close();
	stm.close();
	con.close();
	logger.info("Time to construct signature tree of size "+count+" is "
		    +String.format("%1$.3fs", 
				   1e-3*(System.currentTimeMillis()-start)));
    }

    @Override
    protected Molecule getMol (Object key) {
	Element elm = cache.get(key);
	if (elm != null) {
	    return (Molecule)elm.getObjectValue();
	}

	// load into cache...
	Molecule mol = null;
	if (pool == null) {
	    return mol;
	}

	try {
	    Connection con = pool.getConnection();
	    PreparedStatement pstm = con.prepareStatement(SQLGetMol);
	    pstm.setString(1, (String)key);
	    ResultSet rset = pstm.executeQuery();
	    if (rset.next()) {
		MolHandler mh = new MolHandler (rset.getString("molfile"));
		mh.aromatize();
		mol = mh.getMolecule();
		mol.hydrogenize(false);
		cache.put(new Element (key, mol));
	    }
	    rset.close();
	    pstm.close();
	    con.close();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't retrieve mol for "+key, ex);
	}

	return mol;
    }

    @Override
    public void shutdown () {
	if (pool != null) {
	    pool.shutdown();
	}
	CacheManager.getInstance().shutdown();
	super.shutdown();
    }
}
